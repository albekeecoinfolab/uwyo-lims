require(readxl)
require(tidyverse)


# Try and match the number of sites with the Genomic samples. This is pretty tough given that most aren't using field collector, so this is some best guess stuff. Albeke pulled all known samples from OTU tables, Sam Ewers then tried to pair the Barcodes with the Barcode requestor and potentially sites. We created a spreadsheet that is a mishmash of R, SQL and Graph data queries.

# Get the spreadsheet
dat<- read_excel("./output/UniqueBarcodeFromOTU.xlsx", sheet = "Updated")

unique(dat$BarcodeOwner)
unique(dat$SiteId)
unique(dat$AssumedBarcode)

sampCnt<- dat %>% 
  group_by(BarcodeOwner, SiteId) %>% 
  summarise(Count = n())

siteCnt<- sampCnt %>% 
  group_by(BarcodeOwner) %>% 
  summarise(NumSites = n(),
            NumSamples = sum(Count))

tmp<- sampCnt %>% 
  filter(BarcodeOwner == "Gordon Custer") %>% 
  ungroup() %>% 
  select(SiteId)

paste0(tmp$SiteId, collapse = ", ")

p[order(p)]
261-110
389-262
466-390
